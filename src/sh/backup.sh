#!/bin/bash
###############################################################################
#
# Make a backup of the database.
#
# Usage:
#
#   Options:
#     backup.sh [-k api-key] [-s service-url] [-h]
#
#   Invocation to make a backup:
#     backup.sh
#
###############################################################################
DIR=$(exec 2>/dev/null;cd -- $(dirname "$0"); unset PWD; /usr/bin/pwd || /bin/pwd || pwd)
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

## Load common functions
. "${DIR}/common.sh"



common_getopts
TMPF=$(common_mktemp)



function show_relation {
    local rel_type="$1"
    local src_concept_type="$2"
    local trg_concept_type="$3"

    common_call "/main/graph?edge-relation-type=${rel_type}&source-concept-type=${src_concept_type}&target-concept-type=${trg_concept_type}" > "${TMPF}"
    common_map_printer "${TMPF}" "${src_concept_type}" "${trg_concept_type}"
}



## Backup all concepts
CONCEPT_TYPES=($(common_call '/main/concept/types' | jq '.[]' | tr -d '[,\[\]" ]'))
JOINED_CONCEPT_TYPES=$(common_join_by " " ${CONCEPT_TYPES[*]} | sed 's| |%20|g')
common_call "/main/concepts?type=$JOINED_CONCEPT_TYPES"



## Backup all relations
show_relation broader country continent
show_relation broader municipality region
show_relation broader occupation-name isco-level-4
show_relation broader occupation-name ssyk-level-4
show_relation broader region country
show_relation broader skill skill-headline
show_relation broader sni-level-2 sni-level-1
show_relation broader ssyk-level-2 ssyk-level-1
show_relation broader ssyk-level-3 ssyk-level-2
show_relation broader ssyk-level-4 occupation-field
show_relation broader ssyk-level-4 ssyk-level-3
show_relation broader sun-education-field-2 sun-education-field-1
show_relation broader sun-education-field-3 sun-education-field-2
show_relation broader sun-education-field-4 sun-education-field-3
show_relation broader sun-education-level-2 sun-education-level-1
show_relation broader sun-education-level-3 sun-education-level-2
show_relation related isco-level-4 skill
show_relation related isco-level-4 ssyk-level-4
show_relation related keyword occupation-name
show_relation related occupation-collection occupation-name
show_relation related occupation-name keyword
show_relation related occupation-name occupation-collection
show_relation related skill isco-level-4
show_relation related skill ssyk-level-4
show_relation related ssyk-level-4 isco-level-4
show_relation related ssyk-level-4 skill
