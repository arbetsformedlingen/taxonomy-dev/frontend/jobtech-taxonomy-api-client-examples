#!/bin/bash
###############################################################################
#
# Brute force relation searcher.
#
# Usage:
#
#   Options:
#     relation-searcher.sh [-k api-key] [-s service-url] [-h]
#
#   Invocation to search for relations:
#     relation-searcher.sh
#
#
# Output:
#
#   The output are lines with the format:
#   <number of relations> <relation type> <src concept type> <trg concept type>
#
###############################################################################
DIR=$(exec 2>/dev/null;cd -- $(dirname "$0"); unset PWD; /usr/bin/pwd || /bin/pwd || pwd)
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

## Load common functions
. "${DIR}/common.sh"



common_getopts
TMPF=$(common_mktemp)



## Get all types
CONCEPT_TYPES=($(common_call '/main/concept/types' | jq '.[]' | tr -d '[,\[\]" ]'))
RELATION_TYPES=($(common_call '/main/relation/types' | jq '.[]' | tr -d '[,\[\]" ]'))

for SRC_CONCEPT_TYPE in ${CONCEPT_TYPES[*]}; do
    for TRG_CONCEPT_TYPE in ${CONCEPT_TYPES[*]}; do
        if [ "${SRC_CONCEPT_TYPE}" != "${TRG_CONCEPT_TYPE}" ]; then
            for RELATION_TYPE in ${RELATION_TYPES[*]}; do
                common_call "/main/graph?edge-relation-type=${RELATION_TYPE}&source-concept-type=${SRC_CONCEPT_TYPE}&target-concept-type=${TRG_CONCEPT_TYPE}" >"${TMPF}"
                LEN=$(jq '."taxonomy/graph"."taxonomy/edges" | length' < "${TMPF}")
                if [ "${LEN}" -gt 0 ]; then
                    echo -e "\n${LEN}\t${RELATION_TYPE}\t${SRC_CONCEPT_TYPE}\t${TRG_CONCEPT_TYPE}"
                else
                    echo -n "." >&2
                fi
            done
        fi
    done
done
